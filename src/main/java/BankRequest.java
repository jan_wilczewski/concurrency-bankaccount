/**
 * Created by jan_w on 05.10.2017.
 */
public class BankRequest implements Runnable{

    private TransactionType type;
    private double amount;
    private BankAccount account;

    public BankRequest(TransactionType type, double amount, BankAccount account) {
        this.type = type;
        this.amount = amount;
        this.account = account;
    }

    static int time = 10;
    @Override
    public void run() {
        // long sleepTime = new Random().nextInt(19000) + 1000;
        long sleepTime = time;

        try {
            switch (type){
                case Balance:
                    account.getBalance();
                    break;
                case Add:
                    Thread.sleep(sleepTime);
                    account.addToAccount(amount);
                    System.out.println("Added: " + amount);
                    break;
                case Sub:
                    Thread.sleep(sleepTime);
                    account.substractFromAccount(amount);
                    System.out.println("Subbed: " + amount);
                    break;
            }
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
