import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by jan_w on 05.10.2017.
 */
public class Bank {

    private BankAccount account = new BankAccount();
    private ExecutorService service = Executors.newFixedThreadPool(15);

    public void subFromAccount(double howMuch){
        service.submit(new BankRequest(TransactionType.Sub, howMuch, account));
    }

    public void addToAccount(double howMuch){
        service.submit(new BankRequest(TransactionType.Add, howMuch, account));
    }

    public void balance(){
        service.submit(new BankRequest(TransactionType.Balance, 0.0, account));
    }
}
