/**
 * Created by jan_w on 05.10.2017.
 */
public enum TransactionType {
    Add, Sub, Balance
}
